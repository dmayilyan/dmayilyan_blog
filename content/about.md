---
title: "About"
description: "About me"
---

Hey there!

My name is Davit(d). Some time ago, I decided on a job change, and looking at my web presence, I realized that my true self and interests are totally missing in my web representation. This website is a small step towards the reduction of that gap. It first serves my desire for expression rather than anything else.


This website is a try to store daily data science and programming experiments and Today I Learned (tag [til](https://mayilyan.blog/tags/til/)) type of short posts. I think that bigger projects can speak for themselves and will not be presented here. Moreover, non-technical, philosophical, psychological, sociological, and personal takes on the topics mentioned will also be missing from this website.
