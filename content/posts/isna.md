---
title: "pandas isna()"
date: 2023-09-01T00:17:35+04:00
tags: [python, pandas, til]
math: true
draft: false
---

If you try to check for `np.nan` or `None` using pandas **_isna()_** to get a bool, that is either `True` or `False`. What if we want to get not nan cases? Well, we can add a **_not_** before or a common other solution is a _bitwise Not Operator_ **_~_**. What you would expect is getting the True for False and False for True. That's true for the case of _not_ and it's not true for **_~_**.

```python3
>>> pd.isna(None)
True
>>> not pd.isna(None)
False
>>> ~pd.isna(None)
-2
```

What happened, why did we get -2?

As it turns out `~pd.isna(None)` when exposed to bitwise operation is taken not as a bool but a number representation of `True` which is 1. What does it mean to apply a bitwise not on number one?

$$
\begin{aligned}
    \mathtt{NOT} \quad &0000\\;0000\\;0000\\;0001 \\; \mathsf{(decimal 1)} \\\
        =\quad &1111\\;1111\\;1111\\;1110 \\; \mathsf{(decimal -2)}
\end{aligned}
$$

That's from where -2 came from.