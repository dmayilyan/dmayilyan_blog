---
title: "First post"
date: 2023-08-20T17:22:09+04:00
draft: false
---

As of writing this post I am not sure how the structure and content in this blog will look like. I will integrate my thing along the process of writing.

For now the plan is to have 2 type of contents:

1. Today I Learned (TIL)
2. More in depth technical reflexions of what I find interesting. 

I will try to stick to a minimal count of posts per week, but let's see how it goes ;)
