---
title: "List plotting in mathematica"
date: 2023-08-28T02:10:31+04:00
tags: [mathematica, til]
draft: false
---

While searching for something else in Wasserman stumbled upon [**Legendre polynomials**](https://en.wikipedia.org/wiki/Legendre_polynomials) and decided to have a look at visual representation of it. And why not to do that in Mathematica?!

```Mathematica
(*Defining Legendre polynomial*)
f[j_] := 1/(2^j j!) D[(x^2 - 1)^j, {x, j}]

(*Creating an empty list and appending first 5 polynomials*)
lst = {};
Do[AppendTo[lst, f[j]], {j, 0, 5}]

(*Plotting and styling*)
Plot[lst, {x, -1, 1},
 PlotRange -> {{-1, 1}, {-1, 1}},
 PlotLabel -> Style["Legendre polynomials",
   FontSize -> 16,
   FontWeight -> Bold],
 Background -> Lighter[Black, 0.2],
 ImageSize -> {500, 300}]
```

This perhaps could've been done with a `Table`, but let's keep that for the next time.

And here what we get as a result.

![Legendre polynomials](./legendre.svg "First 6 polynomials")