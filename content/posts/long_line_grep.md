---
title: "Long line grep"
date: 2024-06-17T15:46:42+04:00
tags: [grep, rg, bassh, shell, zsh, til]
draft: false
---
Sometimes when you `grep` or `rg` through a codebase among many others you get a search result that is a huge one-liner and shadows other search results moving them out of the screen. Search result can be a non pretty formatted json or something else. Assuming you want search for **search_string**, preserve the coloring and still be able to see all search results in one terminal screen. Let's assume 200 symbols is enough. Having that in mind we can pipe grep output to `cut`.
```bash
grep -rinI search_string --color=always | cut -c 1-200
```

And of course one can create a custom function that uses grep with the 200 symbol cut. Just put this into your _.bashrc_ or _.bash_profile_ (_.zsh_, _.zsh_profile_ if you use zsh) and you are good to go.
Source you rc file or just relogin with a new terminal and issue is resolved.
```bash
sgrep() {
    grep -rinI --color=always "$1" | cut -c 1-200
}
```
