---
title: "dmenu calculator"
date: 2024-03-05T01:48:17+04:00
tags: [i3, dmenu, manjaro, til]
draft: false
---

For several years already, I have been using a manjaro distro with an i3 tiling windows manager on my home tower. By default, manjaro comes with its own version of dmenu, which is pretty vanilla. What I lack on a daily basis on my screen is an easy-to-access calculator. I tried patching/manually building dmenu to have an integrated calculator, but I ended up in conflicts. For me, it was a bit of a high price to pay for patching and rebuilding endlessly rather than to reach ipython for a simple calculation. Besides, dmenu seems to be the right place to have it.

Today I learned, that one can install a rust port of dmenu that can be extended to have an integrated calculator. First we enforce uninstall with `d` of the built-in dmenu:

```shell
yay -Rdd dmenu-manjaro
```
Clone the [repo](https://github.com/Shizcow/dmenu-rs) from the github (We need the git version for the calc):

```shell
git clone https://github.com/Shizcow/dmenu-rs.git
```
Add calc plugin to `config.mk`:

```shell
PLUGINS = calc
```
Then run:

```shell
make && sudo make install
```

And we would need to add a key binding to `.i3/config` to run the calculator. Here are the 2 key bindings for `dmenu_run` and `dmenu_run --calc`

```shell
bindsym $mod+d exec --no-startup-id dmenu_run
bindsym $mod+equal exec --no-startup-id dmenu_run --calc
```
Now press `$mod` key (Win key in my case) and `=` and voilà, it's alive!
Enjoy!
