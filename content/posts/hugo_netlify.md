---
title: "git submodules in Hugo netlify deployment"
date: 2023-08-24T21:51:03+04:00
tags: [netlify, git]
draft: false
---

As you may have guessed from the title, this blog is currently deployed on [netlify](netlify.com). As they [claim](https://docs.netlify.com/integrations/frameworks/hugo/#hugo-themes) in order to integrate a given theme one should add it as a submodule:

``` sh
cd YOUR_PROJECT_DIRECTORY
git init
git submodule add https://github.com/THEME_CREATOR/THEME_NAME
```

Well, this didn't work for me. What did work was explicit addition of the release, like this

``` sh
git submodule add -f https://github.com/dsrkafuu/hugo-theme-fuji.git@v2.8.2-migurdia themes/fuji
```
In my case *v2.8.2-migurdia* is the latest tagged version of the theme. After this minor touch and following the documentation I got what I wanted.