---
title: "Jupyter %whos"
date: 2023-11-29T15:49:43+04:00
tags: [jupyter, til]
draft: false
---

While creating a model or simply working in jupyter-notebook sometimes there is a need of an overview of variables in the memory. Besides [extensions](https://jupyter-contrib-nbextensions.readthedocs.io/en/latest/nbextensions/varInspector/README.html) which I rarely use, what other options do we have? Apparently `%whos` is a cell magic that combines `locals()`, `shape` for dataframes and `type()` without a need to dig through `locals()` dict keys and values.

Output looks like this:

```
Variable         Type         Data/Info
---------------------------------------
data1_u          DataFrame         0  0.1  0.2       0.<...>n[503 rows x 555 columns]
data1_v          DataFrame         0  0.1  0.2  0.3    <...>n[503 rows x 555 columns]
data2_u          DataFrame         0  0.1  0.2       0.<...>n[503 rows x 555 columns]
data2_v          DataFrame         0  0.1  0.2  0.3    <...>n[503 rows x 555 columns]
filename_pairs   list         n=100
mask             DataFrame         1  1.1  1.2  1.3  1.<...>n[503 rows x 555 columns]
np               module       <module 'numpy' from '/us<...>kages/numpy/__init__.py'>
pd               module       <module 'pandas' from '/u<...>ages/pandas/__init__.py'>
plt              module       <module 'matplotlib.pyplo<...>es/matplotlib/pyplot.py'>
u                str          1u.csv
v                str          1v.csv
```

Nice, no?
